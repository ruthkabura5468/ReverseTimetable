/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reverse.timetable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ruth
 */
public class JTable2 {
     PreparedStatement ps = null;
        Statement stmt;
        ResultSet rs = null;
        
        public Connection getConnection(){
                    Connection con = null;
    try {
      con = DriverManager.getConnection("jdbc:mysql://localhost:3306/rtime", "root", "");
      return con;
    } catch(SQLException ex){
    JOptionPane.showMessageDialog(null, ex);
    return null;
    }
    }
        
        
        public void FillTable () {
        }
        
public void FillTable(JTable table, String Query,JTextField Filter) {
    try {
        Connection con = getConnection();
       ps = con.prepareStatement(Query);
ps.setString(1, "%" + Filter.getText() + "%");
rs = ps.executeQuery();
        
        //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = rs.getMetaData().getColumnCount();
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
        }

        rs.close();
        ps.close();
        con.close();
    }
    catch(SQLException e)
    {
        JOptionPane.showMessageDialog(null,e);
    }
}
}
