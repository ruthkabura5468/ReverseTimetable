/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reverse.timetable;

import java.awt.Color;
import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Ruth
 */
public class Admin_Menu extends javax.swing.JFrame {
    Login log = new Login();
    DBConnect dbconnect = new DBConnect();
    JTable1 table1 = new JTable1();
    JTable2 table2 = new JTable2();
   
    Statement st;
    ResultSet rs;
    PreparedStatement ps;
    String rooms = "SELECT * FROM `room` WHERE `room` LIKE ?";
    String unit = "SELECT * FROM `units` WHERE `UName` LIKE ?";
    String booking = "SELECT `id`, `date`, `lecturer_id`, `room`, BTime FROM `bookings` WHERE `lecturer_id` LIKE ?";
    String filter = "SELECT `id`, `date`, `lecturer_id`, `room`,  BTime FROM `bookings` WHERE STR_TO_DATE(`date`, '%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?, '%Y-%m-%d')";

    /**
     * Creates new form menu
     */
    public Admin_Menu() {
        
    }
    int num = 0;
static String Times[] = {"8.15-9.15","9.15-10.15","10.15-11.15","11.15-12.15","12.15-13.15","13.15-14.15","14.15-15.15","15.15-16.15","16.15-17.15"};
 static String Day[] = {"Monday","Tuesday","Wednesday","Thursday","Friday"};
 static LinkedList<String> RoomsI = new LinkedList<String>();
    String[] noR = {"No rooms"};
     String[] Roomarray = new String[45];
     void init45()
     {
         for(int i = 0;i<45;i++)
         {
             Roomarray[i] = "";
         }
     }
            public void FillARooms(JTable table)
{
       
       init45();
        //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        
      for (int i = 0; i<5; i++)
      {
          ((DefaultTableModel) table.getModel()).addRow(Roomarray);
         
      }
    

 for(int z = 0;z<Roomarray.length;z++)
        {
            Roomarray[z] = null;
        }
    }
            
            void initRoomsT()
 {
      try {
            con1.init();
            String sql = "select room FROM room";
            con1.pst = con1.cn.prepareStatement(sql);
            con1.rs = con1.pst.executeQuery();
            while(con1.rs.next()) 
            {
                String y = con1.rs.getString(1);
                if(RoomsI.contains(y)){}
                else{
                RoomsI.add(y);}
                
            }

            con1.cn.close();
        } catch (SQLException ex ) {
            JOptionPane.showMessageDialog(null, ex);

        }
 }
            
     void getTmtb(String Class)
 {
     //test.setText("");
     for(int i=0; i<Day.length;i++)
     {
       //  test.append(Day[i]+" \n");
         for(int x =0; x<Times.length;x++)
         {
             
         //    test.append(Times[x]+" \n");
             RoomsI.removeAll(RoomsI);
     initRoomsT();
             try {
            con1.init();
            String sql = "select * FROM class where Day = '"+Day[i]+"' and Time = '"+Times[x]+"'";
            
            con1.pst = con1.cn.prepareStatement(sql);
            con1.rs = con1.pst.executeQuery();
            if(con1.rs.next())
            { 
                con1.cn.close();
                    try {
            con1.init();
            //this gets the free classes on the specified day and time, change variables to achieve this
            String sql1 = "select * from class where Day = '"+Day[i]+"' and Time = '"+Times[x]+"'";
            //test.append("yes" +" \n");
            con1.pst = con1.cn.prepareStatement(sql1);
            con1.rs = con1.pst.executeQuery();
            while(con1.rs.next())    
            {
                
                String y = con1.rs.getString("Room");
                String xy = con1.rs.getString("Unit");
                String xyz = con1.rs.getString("Course");
                 String idS = con1.rs.getString("CID");
                if(xyz.equals(Class))
                {
                     jTable5.setValueAt("<html> " +xy+"<br> "+y +"<br> " + ":"+ idS + "." + "</html>", i, x);
                }
             
            }
            con1.cn.close();
        } catch (SQLException ex ) {
            JOptionPane.showMessageDialog(null, ex);
        }       
            }
            con1.cn.close();
        } catch (SQLException ex ) {
            JOptionPane.showMessageDialog(null, ex);
        }  
         }
     }
 }
 Connector con1 = new Connector();
    public Admin_Menu(String name) {
    initComponents();
        jLabel1.setText(name);
      
        jPanel4.setVisible(false);
        txtChar();
        home();
        
    }
    
            // Clear textfields
    public void clrField (JPanel p) {
for (Component C : p.getComponents())
{    
    if (C instanceof JTextField){

        ((JTextComponent) C).setText(""); //abstract superclass
    }
}
}
    
    public void home ()
    {
      
        TPanel(Manager);
        BPanel(History);
        BPanel(Units);
        BPanel(Rooms);
        BPanel(Pass);
        
       
        Scolor(btnManager);
        color(btnHistory);
        color(btnUnits);
        color(btnRooms);
    }
    
    public void txtChar()
    {
        txtCurr.setEchoChar('\u25CF');
        txtConfirm.setEchoChar('\u25CF');
        txtNewPass.setEchoChar('\u25CF');
    }
    
    // Color for selected NavBar item
    public void Scolor (JLabel label) 
    {
    label.setBackground(new Color(67,22,80));
    }
    
    // Color for unselected NavBar items
    public void color (JLabel label)
    {
    label.setBackground(new Color(103,33,122));
    }
    
    // Panel on top
    public void TPanel(JPanel panel)
    {
    panel.setVisible(true);
    }
    
    //Hidden panels
    public void BPanel(JPanel panel)
    {
    panel.setVisible(false);
    }
    
    public void ChangePass(){
        // Changing the current password
        if (txtConfirm.getText().length() > 0 & txtCurr.getText().length() > 0 & txtNewPass.getText().length() > 0) {
            Connection con = dbconnect.connect();
        try {
                String query = "UPDATE `admin` SET `Password` = ? WHERE `Admin_id` = ?";
                PreparedStatement preparedStmt = con.prepareStatement(query);
                String Pass = String.valueOf(txtNewPass.getPassword());
                String ConPass = String.valueOf(txtConfirm.getPassword());
                    if (Pass == null ? ConPass == null : Pass.equals(ConPass)) {
                        preparedStmt.setString(1, Pass);
                        preparedStmt.setString(2, Login.user);

                        // execute the java preparedstatement
                        preparedStmt.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Password Update Successful");
                         txtCurr.setText(null);
                        txtNewPass.setText(null);
                        txtConfirm.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "Passwords do not Match");
                    }
                    
            } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "Password Update was NOT Successful");
             System.out.println(ex);
        } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
                } else {
                    JOptionPane.showMessageDialog(null, "Empty Fields Detected!");
                }
     }
    
     public void FilterTable(JTable table, String query, Date date01, Date date02) {
    Connection con = dbconnect.connect();
    try {
    ps = con.prepareStatement(query);
    java.sql.Timestamp date1 = new java.sql.Timestamp(date01.getTime());
    java.sql.Timestamp date2 = new java.sql.Timestamp(date02.getTime());
    ps.setTimestamp(2, date2);
    ps.setTimestamp(1, date1);
    rs = ps.executeQuery();
     //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = rs.getMetaData().getColumnCount();
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
        }
    } catch (SQLException ex) {
    JOptionPane.showMessageDialog(null, ex);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Unit = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        txtSearch3 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jSeparator14 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        Room = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        txtSearch4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jSeparator15 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jDialog1 = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        txtRoom1 = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtRoom2 = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel42 = new javax.swing.JLabel();
        btnSave1 = new javax.swing.JLabel();
        btnDel1 = new javax.swing.JLabel();
        btnSave3 = new javax.swing.JLabel();
        NavBar = new javax.swing.JPanel();
        btnRooms = new javax.swing.JLabel();
        btnHistory = new javax.swing.JLabel();
        btnManager = new javax.swing.JLabel();
        btnUnits = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        Header = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        Footer = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        Home = new javax.swing.JPanel();
        Manager = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        cboGroup = new javax.swing.JComboBox<>();
        jLabel21 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel38 = new javax.swing.JLabel();
        History = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        txtSearch5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jSeparator16 = new javax.swing.JSeparator();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jDateChooser6 = new com.toedter.calendar.JDateChooser();
        jLabel44 = new javax.swing.JLabel();
        jDateChooser7 = new com.toedter.calendar.JDateChooser();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        Units = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jSeparator12 = new javax.swing.JSeparator();
        txtUnit = new javax.swing.JTextField();
        jSeparator11 = new javax.swing.JSeparator();
        jLabel20 = new javax.swing.JLabel();
        btnReset2 = new javax.swing.JLabel();
        btnUpdate2 = new javax.swing.JLabel();
        btnDel2 = new javax.swing.JLabel();
        btnSave2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        txtSearch2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        Rooms = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtRoom = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        btnSave = new javax.swing.JLabel();
        btnReset = new javax.swing.JLabel();
        btnDel = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JLabel();
        txtCapacity = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel26 = new javax.swing.JLabel();
        cboBuilding = new javax.swing.JComboBox<>();
        txtSearch1 = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        Pass = new javax.swing.JPanel();
        txtConfirm = new javax.swing.JPasswordField();
        jSeparator2 = new javax.swing.JSeparator();
        txtNewPass = new javax.swing.JPasswordField();
        jSeparator1 = new javax.swing.JSeparator();
        txtCurr = new javax.swing.JPasswordField();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel18 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();

        Unit.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Unit.setAlwaysOnTop(true);
        Unit.setBackground(new java.awt.Color(255, 255, 255));
        Unit.setMinimumSize(new java.awt.Dimension(400, 400));
        Unit.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        Unit.setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        txtSearch3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch3.setBorder(null);
        txtSearch3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch3CaretUpdate(evt);
            }
        });
        jPanel1.add(txtSearch3);
        txtSearch3.setBounds(230, 0, 130, 30);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });
        jLabel5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jLabel5KeyReleased(evt);
            }
        });
        jPanel1.add(jLabel5);
        jLabel5.setBounds(360, 0, 30, 30);
        jPanel1.add(jSeparator14);
        jSeparator14.setBounds(220, 30, 170, 10);

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(null);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Unit ID", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setGridColor(new java.awt.Color(153, 153, 153));
        jTable1.setRowHeight(20);
        jTable1.setSelectionBackground(new java.awt.Color(103, 33, 122));
        jTable1.getTableHeader().setResizingAllowed(false);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(130);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(230);
        }

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 40, 380, 350);

        javax.swing.GroupLayout UnitLayout = new javax.swing.GroupLayout(Unit.getContentPane());
        Unit.getContentPane().setLayout(UnitLayout);
        UnitLayout.setHorizontalGroup(
            UnitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        UnitLayout.setVerticalGroup(
            UnitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );

        Room.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Room.setAlwaysOnTop(true);
        Room.setBackground(new java.awt.Color(255, 255, 255));
        Room.setMinimumSize(new java.awt.Dimension(400, 400));
        Room.setModal(true);
        Room.setResizable(false);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        txtSearch4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch4.setBorder(null);
        txtSearch4.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch4CaretUpdate(evt);
            }
        });
        jPanel2.add(txtSearch4);
        txtSearch4.setBounds(230, 0, 130, 30);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel6);
        jLabel6.setBounds(360, 0, 30, 30);
        jPanel2.add(jSeparator15);
        jSeparator15.setBounds(220, 30, 170, 10);

        jScrollPane4.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane4.setBorder(null);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Room", "Building", "Capacity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable4.setGridColor(new java.awt.Color(153, 153, 153));
        jTable4.setRowHeight(20);
        jTable4.setSelectionBackground(new java.awt.Color(103, 33, 122));
        jTable4.getTableHeader().setResizingAllowed(false);
        jTable4.getTableHeader().setReorderingAllowed(false);
        jTable4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable4MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jTable4);
        if (jTable4.getColumnModel().getColumnCount() > 0) {
            jTable4.getColumnModel().getColumn(0).setResizable(false);
            jTable4.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable4.getColumnModel().getColumn(1).setResizable(false);
            jTable4.getColumnModel().getColumn(1).setPreferredWidth(130);
            jTable4.getColumnModel().getColumn(2).setResizable(false);
            jTable4.getColumnModel().getColumn(2).setPreferredWidth(230);
            jTable4.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel2.add(jScrollPane4);
        jScrollPane4.setBounds(10, 40, 380, 350);

        javax.swing.GroupLayout RoomLayout = new javax.swing.GroupLayout(Room.getContentPane());
        Room.getContentPane().setLayout(RoomLayout);
        RoomLayout.setHorizontalGroup(
            RoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        RoomLayout.setVerticalGroup(
            RoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );

        jDialog1.setMinimumSize(new java.awt.Dimension(387, 265));
        jDialog1.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        jDialog1.setUndecorated(true);
        jDialog1.setResizable(false);

        jPanel3.setLayout(null);

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setText("Unit");
        jPanel3.add(jLabel31);
        jLabel31.setBounds(40, 40, 24, 20);

        txtRoom1.setEditable(false);
        txtRoom1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtRoom1.setBorder(null);
        txtRoom1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoom1ActionPerformed(evt);
            }
        });
        jPanel3.add(txtRoom1);
        txtRoom1.setBounds(100, 40, 210, 30);
        jPanel3.add(jSeparator4);
        jSeparator4.setBounds(90, 70, 240, 10);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        jLabel12.setText("jLabel8");
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel12MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel12);
        jLabel12.setBounds(310, 40, 20, 30);

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel41.setText("Room");
        jPanel3.add(jLabel41);
        jLabel41.setBounds(40, 100, 37, 20);

        txtRoom2.setEditable(false);
        txtRoom2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtRoom2.setBorder(null);
        txtRoom2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoom2ActionPerformed(evt);
            }
        });
        jPanel3.add(txtRoom2);
        txtRoom2.setBounds(100, 100, 210, 30);
        jPanel3.add(jSeparator8);
        jSeparator8.setBounds(90, 130, 240, 10);

        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        jLabel42.setText("jLabel8");
        jLabel42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel42MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel42);
        jLabel42.setBounds(310, 100, 20, 30);

        btnSave1.setBackground(new java.awt.Color(255, 169, 2));
        btnSave1.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        btnSave1.setForeground(new java.awt.Color(255, 255, 255));
        btnSave1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave1.setText("Save");
        btnSave1.setOpaque(true);
        btnSave1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSave1MouseClicked(evt);
            }
        });
        jPanel3.add(btnSave1);
        btnSave1.setBounds(50, 150, 100, 30);

        btnDel1.setBackground(new java.awt.Color(204, 204, 204));
        btnDel1.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        btnDel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnDel1.setText("Cancel");
        btnDel1.setOpaque(true);
        btnDel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDel1MouseClicked(evt);
            }
        });
        jPanel3.add(btnDel1);
        btnDel1.setBounds(200, 150, 100, 30);

        btnSave3.setBackground(new java.awt.Color(255, 0, 0));
        btnSave3.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        btnSave3.setForeground(new java.awt.Color(255, 255, 255));
        btnSave3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave3.setText("Delete");
        btnSave3.setOpaque(true);
        btnSave3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSave3MouseClicked(evt);
            }
        });
        jPanel3.add(btnSave3);
        btnSave3.setBounds(120, 200, 100, 30);

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(222, 222, 222));
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(1366, 760));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(1366, 760));
        getContentPane().setLayout(null);

        NavBar.setBackground(new java.awt.Color(103, 33, 122));
        NavBar.setLayout(null);

        btnRooms.setBackground(new java.awt.Color(103, 33, 122));
        btnRooms.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnRooms.setForeground(new java.awt.Color(255, 255, 255));
        btnRooms.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Google Classroom Filled-20.png"))); // NOI18N
        btnRooms.setLabelFor(NavBar);
        btnRooms.setText("Rooms");
        btnRooms.setOpaque(true);
        btnRooms.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRoomsMouseClicked(evt);
            }
        });
        NavBar.add(btnRooms);
        btnRooms.setBounds(0, 170, 230, 50);

        btnHistory.setBackground(new java.awt.Color(103, 33, 122));
        btnHistory.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnHistory.setForeground(new java.awt.Color(255, 255, 255));
        btnHistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Calendar Filled-20.png"))); // NOI18N
        btnHistory.setLabelFor(NavBar);
        btnHistory.setText("Booking History");
        btnHistory.setOpaque(true);
        btnHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnHistoryMouseClicked(evt);
            }
        });
        NavBar.add(btnHistory);
        btnHistory.setBounds(0, 70, 230, 50);

        btnManager.setBackground(new java.awt.Color(103, 33, 122));
        btnManager.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnManager.setForeground(new java.awt.Color(255, 255, 255));
        btnManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Calendar Plus Filled-20.png"))); // NOI18N
        btnManager.setLabelFor(NavBar);
        btnManager.setText("Manage Timetables");
        btnManager.setOpaque(true);
        btnManager.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnManagerMouseClicked(evt);
            }
        });
        NavBar.add(btnManager);
        btnManager.setBounds(0, 20, 230, 50);

        btnUnits.setBackground(new java.awt.Color(103, 33, 122));
        btnUnits.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUnits.setForeground(new java.awt.Color(255, 255, 255));
        btnUnits.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Course Filled-20.png"))); // NOI18N
        btnUnits.setLabelFor(NavBar);
        btnUnits.setText("Units");
        btnUnits.setOpaque(true);
        btnUnits.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUnitsMouseClicked(evt);
            }
        });
        NavBar.add(btnUnits);
        btnUnits.setBounds(0, 120, 230, 50);

        getContentPane().add(NavBar);
        NavBar.setBounds(0, 79, 230, 690);

        jPanel4.setBackground(new java.awt.Color(230, 230, 230));
        jPanel4.setLayout(null);

        jLabel47.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel47.setText("Change Password");
        jLabel47.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel47MouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel47MouseMoved(evt);
            }
        });
        jLabel47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel47MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel47);
        jLabel47.setBounds(0, 0, 170, 30);

        jLabel48.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel48.setText("Logout");
        jLabel48.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel48MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel48);
        jLabel48.setBounds(0, 30, 170, 30);

        getContentPane().add(jPanel4);
        jPanel4.setBounds(1180, 80, 170, 60);

        Header.setBackground(new java.awt.Color(103, 33, 122));
        Header.setLayout(null);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 32)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Minimize Window Filled-20.png"))); // NOI18N
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
        });
        Header.add(jLabel10);
        jLabel10.setBounds(1300, 0, 20, 20);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("User Name");
        Header.add(jLabel1);
        jLabel1.setBounds(870, 40, 455, 40);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 32)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Close Window Filled-20.png"))); // NOI18N
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });
        Header.add(jLabel9);
        jLabel9.setBounds(1330, 0, 20, 20);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-expand-arrow.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        Header.add(jLabel2);
        jLabel2.setBounds(1330, 50, 20, 20);

        jLabel19.setFont(new java.awt.Font("Comic Sans MS", 0, 28)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Reverse Timetable System");
        Header.add(jLabel19);
        jLabel19.setBounds(70, 0, 430, 40);

        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-rating.png"))); // NOI18N
        Header.add(jLabel43);
        jLabel43.setBounds(20, 0, 50, 50);

        getContentPane().add(Header);
        Header.setBounds(0, 0, 1370, 80);

        Footer.setBackground(new java.awt.Color(255, 255, 255));

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Reverse Timetable System");

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("2017");

        javax.swing.GroupLayout FooterLayout = new javax.swing.GroupLayout(Footer);
        Footer.setLayout(FooterLayout);
        FooterLayout.setHorizontalGroup(
            FooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterLayout.createSequentialGroup()
                .addContainerGap(446, Short.MAX_VALUE)
                .addGroup(FooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(446, Short.MAX_VALUE))
        );
        FooterLayout.setVerticalGroup(
            FooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterLayout.createSequentialGroup()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addGap(0, 26, Short.MAX_VALUE))
        );

        getContentPane().add(Footer);
        Footer.setBounds(250, 680, 1120, 60);

        Home.setBackground(new java.awt.Color(255, 255, 255));
        Home.setLayout(new java.awt.CardLayout());

        Manager.setBackground(new java.awt.Color(255, 255, 255));
        Manager.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel25.setBackground(new java.awt.Color(255, 255, 255));
        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(153, 153, 153));
        jLabel25.setText("Manage Timetables");
        Manager.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 0, 299, 38));

        cboGroup.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cboGroup.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ICS A", "ICS B", "ICS 2A", "ICS 2B", "ICS 2C", "ICS 2 EX", "ICS 3", "BIF 4" }));
        cboGroup.setBorder(null);
        cboGroup.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboGroupItemStateChanged(evt);
            }
        });
        cboGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboGroupActionPerformed(evt);
            }
        });
        Manager.add(cboGroup, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 60, 230, 30));

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setText("Group");
        Manager.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 50, 30));

        jLabel27.setBackground(new java.awt.Color(255, 255, 255));
        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("Friday");
        Manager.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 415, 80, 70));

        jLabel28.setBackground(new java.awt.Color(255, 255, 255));
        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("Thursday");
        Manager.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 345, 80, 70));

        jLabel33.setBackground(new java.awt.Color(255, 255, 255));
        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("Tuesday");
        Manager.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 205, 80, 70));

        jLabel34.setBackground(new java.awt.Color(255, 255, 255));
        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel34.setText("Wednesday");
        Manager.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 275, 80, 70));

        jLabel35.setBackground(new java.awt.Color(255, 255, 255));
        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel35.setText("Monday");
        Manager.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 135, 80, 70));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setBorder(null);
        jScrollPane5.setHorizontalScrollBar(null);

        jTable5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "08:15 - 09-15", "09:15 - 10:15", "10:15 - 11:15", "11:15 - 12:15", "12:15 - 13:15", "13:15 - 14:15", "14:15 - 15:15", "15:15 - 16:15", "16:15 - 17:15"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable5.setAutoscrolls(false);
        jTable5.setCellSelectionEnabled(true);
        jTable5.setGridColor(new java.awt.Color(204, 204, 204));
        jTable5.setRowHeight(70);
        jTable5.setSurrendersFocusOnKeystroke(true);
        jTable5.getTableHeader().setResizingAllowed(false);
        jTable5.getTableHeader().setReorderingAllowed(false);
        jTable5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable5MouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(jTable5);
        jTable5.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        if (jTable5.getColumnModel().getColumnCount() > 0) {
            jTable5.getColumnModel().getColumn(0).setResizable(false);
            jTable5.getColumnModel().getColumn(1).setResizable(false);
            jTable5.getColumnModel().getColumn(2).setResizable(false);
            jTable5.getColumnModel().getColumn(3).setResizable(false);
            jTable5.getColumnModel().getColumn(4).setResizable(false);
            jTable5.getColumnModel().getColumn(5).setResizable(false);
            jTable5.getColumnModel().getColumn(6).setResizable(false);
            jTable5.getColumnModel().getColumn(7).setResizable(false);
            jTable5.getColumnModel().getColumn(8).setResizable(false);
        }

        Manager.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 112, 1020, 380));

        jLabel38.setBackground(new java.awt.Color(233, 30, 24));
        jLabel38.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("Delete Timetables");
        jLabel38.setOpaque(true);
        jLabel38.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel38MouseClicked(evt);
            }
        });
        Manager.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 60, 190, 35));

        Home.add(Manager, "card9");

        History.setBackground(new java.awt.Color(255, 255, 255));
        History.setLayout(null);

        jLabel24.setBackground(new java.awt.Color(255, 255, 255));
        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(153, 153, 153));
        jLabel24.setText("Booking History");
        History.add(jLabel24);
        jLabel24.setBounds(18, 0, 299, 38);

        txtSearch5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch5.setBorder(null);
        txtSearch5.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch5CaretUpdate(evt);
            }
        });
        History.add(txtSearch5);
        txtSearch5.setBounds(90, 60, 210, 30);

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        History.add(jLabel7);
        jLabel7.setBounds(300, 60, 30, 30);
        History.add(jSeparator16);
        jSeparator16.setBounds(80, 90, 250, 10);

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setBorder(null);
        jScrollPane6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jScrollPane6.setOpaque(false);

        jTable6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Date", "Lecturer", "Room", "Book Time"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable6.setGridColor(new java.awt.Color(255, 255, 255));
        jTable6.setOpaque(false);
        jTable6.setRowHeight(22);
        jTable6.setRowSelectionAllowed(false);
        jTable6.setSelectionBackground(new java.awt.Color(103, 33, 122));
        jTable6.getTableHeader().setResizingAllowed(false);
        jTable6.getTableHeader().setReorderingAllowed(false);
        jTable6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable6MouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jTable6);
        if (jTable6.getColumnModel().getColumnCount() > 0) {
            jTable6.getColumnModel().getColumn(0).setResizable(false);
            jTable6.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTable6.getColumnModel().getColumn(1).setResizable(false);
            jTable6.getColumnModel().getColumn(1).setPreferredWidth(150);
            jTable6.getColumnModel().getColumn(2).setResizable(false);
            jTable6.getColumnModel().getColumn(3).setResizable(false);
            jTable6.getColumnModel().getColumn(4).setResizable(false);
        }

        History.add(jScrollPane6);
        jScrollPane6.setBounds(0, 100, 1120, 460);

        jDateChooser6.setBackground(new java.awt.Color(255, 255, 255));
        jDateChooser6.setDateFormatString("yyyy-MM-dd");
        jDateChooser6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jDateChooser6.setMinSelectableDate(new java.util.Date(-62135776709000L));
        History.add(jDateChooser6);
        jDateChooser6.setBounds(460, 60, 180, 30);

        jLabel44.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel44.setText("From");
        History.add(jLabel44);
        jLabel44.setBounds(410, 60, 40, 20);

        jDateChooser7.setBackground(new java.awt.Color(255, 255, 255));
        jDateChooser7.setDateFormatString("yyyy-MM-dd");
        jDateChooser7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jDateChooser7.setMinSelectableDate(new java.util.Date(-62135776709000L));
        History.add(jDateChooser7);
        jDateChooser7.setBounds(730, 60, 180, 30);

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel45.setText("Date");
        jLabel45.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel45MouseClicked(evt);
            }
        });
        History.add(jLabel45);
        jLabel45.setBounds(930, 60, 40, 30);

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel46.setText("Date");
        History.add(jLabel46);
        jLabel46.setBounds(680, 60, 40, 20);

        Home.add(History, "card8");

        Units.setBackground(new java.awt.Color(255, 255, 255));
        Units.setLayout(null);

        jLabel37.setBackground(new java.awt.Color(255, 255, 255));
        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(153, 153, 153));
        jLabel37.setText("Units");
        Units.add(jLabel37);
        jLabel37.setBounds(18, 0, 299, 38);

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel36.setText("Unit ID");
        Units.add(jLabel36);
        jLabel36.setBounds(50, 120, 70, 20);

        txtID.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtID.setBorder(null);
        txtID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIDActionPerformed(evt);
            }
        });
        Units.add(txtID);
        txtID.setBounds(130, 110, 270, 30);
        Units.add(jSeparator12);
        jSeparator12.setBounds(120, 140, 280, 10);

        txtUnit.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtUnit.setBorder(null);
        txtUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUnitActionPerformed(evt);
            }
        });
        Units.add(txtUnit);
        txtUnit.setBounds(130, 170, 270, 30);
        Units.add(jSeparator11);
        jSeparator11.setBounds(120, 200, 280, 10);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Unit Name");
        Units.add(jLabel20);
        jLabel20.setBounds(50, 180, 70, 20);

        btnReset2.setBackground(new java.awt.Color(103, 33, 122));
        btnReset2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnReset2.setForeground(new java.awt.Color(255, 255, 255));
        btnReset2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnReset2.setText("Reset");
        btnReset2.setOpaque(true);
        btnReset2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnReset2MouseClicked(evt);
            }
        });
        Units.add(btnReset2);
        btnReset2.setBounds(40, 250, 170, 40);

        btnUpdate2.setBackground(new java.awt.Color(47, 201, 80));
        btnUpdate2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnUpdate2.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdate2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnUpdate2.setText("Update");
        btnUpdate2.setOpaque(true);
        btnUpdate2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUpdate2MouseClicked(evt);
            }
        });
        Units.add(btnUpdate2);
        btnUpdate2.setBounds(40, 310, 170, 40);

        btnDel2.setBackground(new java.awt.Color(233, 30, 24));
        btnDel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnDel2.setForeground(new java.awt.Color(255, 255, 255));
        btnDel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnDel2.setText("Delete");
        btnDel2.setOpaque(true);
        btnDel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDel2MouseClicked(evt);
            }
        });
        Units.add(btnDel2);
        btnDel2.setBounds(230, 310, 170, 40);

        btnSave2.setBackground(new java.awt.Color(255, 169, 2));
        btnSave2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnSave2.setForeground(new java.awt.Color(255, 255, 255));
        btnSave2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave2.setText("Save");
        btnSave2.setOpaque(true);
        btnSave2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSave2MouseClicked(evt);
            }
        });
        Units.add(btnSave2);
        btnSave2.setBounds(230, 250, 170, 40);

        jScrollPane3.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane3.setBorder(null);
        jScrollPane3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jScrollPane3.setOpaque(false);

        jTable3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Unit ID", "Unit"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.setGridColor(new java.awt.Color(255, 255, 255));
        jTable3.setOpaque(false);
        jTable3.setRowHeight(22);
        jTable3.setSelectionBackground(new java.awt.Color(103, 33, 122));
        jTable3.getTableHeader().setResizingAllowed(false);
        jTable3.getTableHeader().setReorderingAllowed(false);
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
            jTable3.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable3.getColumnModel().getColumn(1).setResizable(false);
            jTable3.getColumnModel().getColumn(1).setPreferredWidth(50);
            jTable3.getColumnModel().getColumn(2).setResizable(false);
            jTable3.getColumnModel().getColumn(2).setPreferredWidth(200);
        }

        Units.add(jScrollPane3);
        jScrollPane3.setBounds(480, 100, 640, 460);

        txtSearch2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch2.setBorder(null);
        txtSearch2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch2CaretUpdate(evt);
            }
        });
        Units.add(txtSearch2);
        txtSearch2.setBounds(870, 60, 200, 30);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        Units.add(jLabel4);
        jLabel4.setBounds(1070, 60, 30, 30);
        Units.add(jSeparator10);
        jSeparator10.setBounds(860, 90, 240, 10);

        Home.add(Units, "card8");

        Rooms.setBackground(new java.awt.Color(255, 255, 255));
        Rooms.setLayout(null);

        jLabel23.setBackground(new java.awt.Color(255, 255, 255));
        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(153, 153, 153));
        jLabel23.setText("Rooms");
        Rooms.add(jLabel23);
        jLabel23.setBounds(18, 0, 299, 38);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Building");
        Rooms.add(jLabel13);
        jLabel13.setBounds(60, 170, 60, 20);

        txtRoom.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtRoom.setBorder(null);
        txtRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoomActionPerformed(evt);
            }
        });
        Rooms.add(txtRoom);
        txtRoom.setBounds(140, 90, 230, 30);
        Rooms.add(jSeparator5);
        jSeparator5.setBounds(130, 120, 240, 10);

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setBorder(null);
        jScrollPane2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jScrollPane2.setOpaque(false);

        jTable2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Room", "Building", "Capacity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.setGridColor(new java.awt.Color(255, 255, 255));
        jTable2.setOpaque(false);
        jTable2.setRowHeight(22);
        jTable2.setSelectionBackground(new java.awt.Color(103, 33, 122));
        jTable2.getTableHeader().setResizingAllowed(false);
        jTable2.getTableHeader().setReorderingAllowed(false);
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable2MouseEntered(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable2.getColumnModel().getColumn(2).setResizable(false);
            jTable2.getColumnModel().getColumn(2).setPreferredWidth(150);
            jTable2.getColumnModel().getColumn(3).setResizable(false);
        }

        Rooms.add(jScrollPane2);
        jScrollPane2.setBounds(480, 100, 640, 460);

        btnSave.setBackground(new java.awt.Color(255, 169, 2));
        btnSave.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("Save");
        btnSave.setOpaque(true);
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
        });
        Rooms.add(btnSave);
        btnSave.setBounds(240, 350, 170, 40);

        btnReset.setBackground(new java.awt.Color(103, 33, 122));
        btnReset.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnReset.setForeground(new java.awt.Color(255, 255, 255));
        btnReset.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnReset.setText("Reset");
        btnReset.setOpaque(true);
        btnReset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnResetMouseClicked(evt);
            }
        });
        Rooms.add(btnReset);
        btnReset.setBounds(30, 350, 170, 40);

        btnDel.setBackground(new java.awt.Color(233, 30, 24));
        btnDel.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnDel.setForeground(new java.awt.Color(255, 255, 255));
        btnDel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnDel.setText("Delete");
        btnDel.setOpaque(true);
        btnDel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDelMouseClicked(evt);
            }
        });
        Rooms.add(btnDel);
        btnDel.setBounds(240, 420, 170, 40);

        btnUpdate.setBackground(new java.awt.Color(47, 201, 80));
        btnUpdate.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnUpdate.setText("Update");
        btnUpdate.setOpaque(true);
        btnUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUpdateMouseClicked(evt);
            }
        });
        Rooms.add(btnUpdate);
        btnUpdate.setBounds(30, 420, 170, 40);

        txtCapacity.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCapacity.setBorder(null);
        txtCapacity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCapacityActionPerformed(evt);
            }
        });
        Rooms.add(txtCapacity);
        txtCapacity.setBounds(140, 230, 230, 30);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Capacity");
        Rooms.add(jLabel16);
        jLabel16.setBounds(60, 240, 60, 20);
        Rooms.add(jSeparator6);
        jSeparator6.setBounds(130, 260, 240, 10);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel26.setText("Room");
        Rooms.add(jLabel26);
        jLabel26.setBounds(60, 100, 60, 20);

        cboBuilding.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cboBuilding.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Management Science Bulding", "Phase 1", "Sir Thomas More Building", "Strathmore Business School", "Strathmore Law School" }));
        cboBuilding.setBorder(null);
        cboBuilding.setOpaque(false);
        Rooms.add(cboBuilding);
        cboBuilding.setBounds(130, 160, 240, 30);

        txtSearch1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch1.setBorder(null);
        txtSearch1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch1CaretUpdate(evt);
            }
        });
        Rooms.add(txtSearch1);
        txtSearch1.setBounds(870, 60, 200, 30);
        Rooms.add(jSeparator7);
        jSeparator7.setBounds(860, 90, 240, 10);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Search-20.png"))); // NOI18N
        Rooms.add(jLabel3);
        jLabel3.setBounds(1070, 60, 30, 30);

        Home.add(Rooms, "card7");

        Pass.setBackground(new java.awt.Color(255, 255, 255));
        Pass.setLayout(null);

        txtConfirm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtConfirm.setBorder(null);
        Pass.add(txtConfirm);
        txtConfirm.setBounds(470, 220, 240, 30);
        Pass.add(jSeparator2);
        jSeparator2.setBounds(470, 250, 240, 10);

        txtNewPass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNewPass.setBorder(null);
        Pass.add(txtNewPass);
        txtNewPass.setBounds(470, 170, 240, 30);
        Pass.add(jSeparator1);
        jSeparator1.setBounds(470, 200, 240, 10);

        txtCurr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCurr.setBorder(null);
        Pass.add(txtCurr);
        txtCurr.setBounds(470, 120, 240, 30);
        Pass.add(jSeparator3);
        jSeparator3.setBounds(470, 150, 240, 10);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Current Password");
        Pass.add(jLabel11);
        jLabel11.setBounds(340, 130, 120, 20);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("New Password");
        Pass.add(jLabel8);
        jLabel8.setBounds(340, 180, 110, 20);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Confirm Password");
        Pass.add(jLabel17);
        jLabel17.setBounds(340, 230, 120, 20);

        jCheckBox1.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jCheckBox1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-Show Password-20.png"))); // NOI18N
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        Pass.add(jCheckBox1);
        jCheckBox1.setBounds(710, 220, 30, 30);

        jLabel18.setBackground(new java.awt.Color(255, 255, 255));
        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(153, 153, 153));
        jLabel18.setText("Change Password");
        Pass.add(jLabel18);
        jLabel18.setBounds(18, 0, 299, 38);

        jLabel32.setBackground(new java.awt.Color(255, 169, 2));
        jLabel32.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("Update Password");
        jLabel32.setOpaque(true);
        jLabel32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel32MouseClicked(evt);
            }
        });
        Pass.add(jLabel32);
        jLabel32.setBounds(410, 300, 230, 40);

        Home.add(Pass, "card2");

        getContentPane().add(Home);
        Home.setBounds(250, 100, 1120, 560);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        // TODO add your handling code here:
        this.setExtendedState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel10MouseClicked

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
        if (jCheckBox1.isSelected()){
            txtCurr.setEchoChar((char)0);
            txtConfirm.setEchoChar((char)0);
            txtNewPass.setEchoChar((char)0);
        } else{
            txtCurr.setEchoChar('\u25CF');
            txtConfirm.setEchoChar('\u25CF');
            txtNewPass.setEchoChar('\u25CF');
        }
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jLabel32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel32MouseClicked
        // TODO add your handling code here:
        if (txtCurr.getText().length() > 0 & txtConfirm.getText().length() > 0 & txtNewPass.getText().length() > 0) {
            Connection con = dbconnect.connect();
            try {
                ps = con.prepareStatement("SELECT * FROM `admin` WHERE BINARY `Admin_id`=? AND BINARY `Password`=?");
                ps.setString(1, Login.user);
                ps.setString(2, String.valueOf(txtCurr.getPassword()));
                ResultSet result = ps.executeQuery();
                if (result.next()) {
                    ChangePass();
                    home();
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrect Password");
                    System.out.println(Login.user);
                }
                
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Password Update was Unsuccessful");
                System.out.println(ex);
            } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
        } else {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected!");
            }
    }//GEN-LAST:event_jLabel32MouseClicked

    private void btnManagerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnManagerMouseClicked
        // TODO add your handling code here:
        
        TPanel(Manager);
        BPanel(History);
        BPanel(Units);
        BPanel(Rooms);
        BPanel(Pass);
        
       
        Scolor(btnManager);
        color(btnHistory);
        color(btnUnits);
        color(btnRooms);
        FillARooms(jTable5);
        getTmtb(cboGroup.getSelectedItem().toString());
        
    }//GEN-LAST:event_btnManagerMouseClicked

    private void btnHistoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHistoryMouseClicked
        // TODO add your handling code here:
        table2.FillTable(jTable6, booking, txtSearch5);
        
        BPanel(Manager);
        TPanel(History);
        BPanel(Units);
        BPanel(Rooms);
        BPanel(Pass);
        
        
        color(btnManager);
        Scolor(btnHistory);
        color(btnRooms);
        color(btnUnits);
    }//GEN-LAST:event_btnHistoryMouseClicked

    private void btnRoomsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRoomsMouseClicked
        // TODO add your handling code here:
        table2.FillTable(jTable2, rooms, txtSearch2);
        
        
        BPanel(Manager);
        BPanel(History);
        BPanel(Units);
        TPanel(Rooms);
        BPanel(Pass);
        
        
        color(btnManager);
        color(btnHistory);
        color(btnUnits);
        Scolor(btnRooms);
    }//GEN-LAST:event_btnRoomsMouseClicked

    private void txtRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRoomActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
        int selectedRowIndex = jTable2.getSelectedRow();
        txtRoom.setText(model.getValueAt(selectedRowIndex, 1).toString());
        cboBuilding.setSelectedItem(model.getValueAt(selectedRowIndex, 2).toString());
        txtCapacity.setText(model.getValueAt(selectedRowIndex, 3).toString());
    }//GEN-LAST:event_jTable2MouseClicked

    private void btnResetMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResetMouseClicked
        // TODO add your handling code here:
        clrField(Rooms);
    }//GEN-LAST:event_btnResetMouseClicked

    private void txtCapacityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCapacityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCapacityActionPerformed

    private void txtSearch1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch1CaretUpdate
        // TODO add your handling code here:
         table2.FillTable(jTable2, rooms, txtSearch1);
    }//GEN-LAST:event_txtSearch1CaretUpdate

    private void btnDelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDelMouseClicked
        // TODO add your handling code here:
        Connection con = dbconnect.connect();
        DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
        int selectedRowIndex = jTable2.getSelectedRow();
        try {
            ps = con.prepareStatement("DELETE FROM `room` WHERE `ID` = ?");
            ps.setString(1, model.getValueAt(selectedRowIndex, 0).toString());
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Room Deleted Successfully");
            
            table2.FillTable(jTable2, rooms, txtSearch1);
            
            txtRoom.setText(null);
            cboBuilding.setSelectedIndex(0);
            txtCapacity.setText(null);
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
    }//GEN-LAST:event_btnDelMouseClicked

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        // TODO add your handling code here:
        if (txtRoom.getText().equals("") || txtCapacity.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Some Fields are Empty");
        } else 
        {
            Connection con = dbconnect.connect();
        try {
                        ps = con.prepareStatement("INSERT INTO `room`(`room`, `building`, `capacity`)" + "VALUES (?, ?, ?)");
                        ps.setString (1, txtRoom.getText());
                        ps.setObject(2,cboBuilding.getSelectedItem());
                        ps.setString (3, txtCapacity.getText());
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Record Saved Successfully");
                        table2.FillTable(jTable2, rooms, txtSearch1);
                        
                        txtRoom.setText(null);
                        cboBuilding.setSelectedIndex(0);
                        txtCapacity.setText(null);
                        
                          } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnUpdateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseClicked
        // TODO add your handling code here:
        if (txtRoom.getText().equals("") || txtCapacity.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Some Fields are Empty");
        } else 
        {
            Connection con = dbconnect.connect();
            DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
        int selectedRowIndex = jTable2.getSelectedRow();
        try {
         String query = "UPDATE `room` SET `room`=?,`building`=?,`capacity`=? WHERE `ID` = ?";
                ps = con.prepareStatement(query);
                ps.setObject(2, cboBuilding.getSelectedItem());
                ps.setString(3, txtCapacity.getText());
                ps.setString(1, txtRoom.getText());
                ps.setString(4, model.getValueAt(selectedRowIndex, 0).toString());
                ps.executeUpdate();
                
                        JOptionPane.showMessageDialog(null, "Record Updated Successfully");
                        
                         table2.FillTable(jTable2, rooms, txtSearch1);
                        txtRoom.setText(null);
                        cboBuilding.setSelectedIndex(0);
                        txtCapacity.setText(null);
                        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }//GEN-LAST:event_btnUpdateMouseClicked

    private void cboGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboGroupActionPerformed
        // TODO add your handling code here:
         FillARooms(jTable5);
         getTmtb(cboGroup.getSelectedItem().toString());
    }//GEN-LAST:event_cboGroupActionPerformed

    private void cboGroupItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboGroupItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cboGroupItemStateChanged

    private void btnUnitsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUnitsMouseClicked
        // TODO add your handling code here:
        
        BPanel(Manager);
        BPanel(History);
        TPanel(Units);
        BPanel(Rooms);
        BPanel(Pass);
        
      
        color(btnManager);
        color(btnHistory);
        Scolor(btnUnits);
        color(btnRooms);
    }//GEN-LAST:event_btnUnitsMouseClicked

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        int selectedRowIndex = jTable3.getSelectedRow();
        txtID.setText(model.getValueAt(selectedRowIndex, 1).toString());
        txtUnit.setText(model.getValueAt(selectedRowIndex, 2).toString());
    }//GEN-LAST:event_jTable3MouseClicked

    private void txtSearch2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch2CaretUpdate
        // TODO add your handling code here:
        table2.FillTable(jTable3, unit, txtSearch2);
    }//GEN-LAST:event_txtSearch2CaretUpdate

    private void btnReset2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnReset2MouseClicked
        // TODO add your handling code here:
        clrField(Units);
    }//GEN-LAST:event_btnReset2MouseClicked

    private void btnUpdate2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdate2MouseClicked
        // TODO add your handling code here:
          if (txtID.getText().equals("") || txtUnit.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Some Fields are Empty");
        } else 
        {
            Connection con = dbconnect.connect();
            DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
            int selectedRowIndex = jTable3.getSelectedRow();
        try {
         String query = "UPDATE `units` SET `UID`=?,`UName`=? WHERE `ID` = ?";
                ps = con.prepareStatement(query);
                ps.setString(1, txtID.getText());
                ps.setString(2, txtUnit.getText());
                ps.setString(3, model.getValueAt(selectedRowIndex, 0).toString());
                ps.executeUpdate();
                
                        JOptionPane.showMessageDialog(null, "Record Updated Successfully");
                        
                         table2.FillTable(jTable3, unit, txtSearch2);
                        clrField(Units);
                        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }//GEN-LAST:event_btnUpdate2MouseClicked

    private void btnDel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel2MouseClicked
        // TODO add your handling code here:
        Connection con = dbconnect.connect();
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        int selectedRowIndex = jTable3.getSelectedRow();
        try {
            ps = con.prepareStatement("DELETE FROM `units` WHERE `ID` = ?");
            ps.setString(1, model.getValueAt(selectedRowIndex, 0).toString());
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Unit Deleted Successfully");
            
            table2.FillTable(jTable3, unit, txtSearch2);
            
            clrField(Units);
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
    }//GEN-LAST:event_btnDel2MouseClicked

    private void btnSave2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave2MouseClicked
        // TODO add your handling code here:
        if (txtID.getText().equals("") || txtUnit.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Some Fields are Empty");
        } else 
        {
            Connection con = dbconnect.connect();
        try {
                        ps = con.prepareStatement("INSERT INTO `units`(`UID`, `UName`) VALUES (?, ?)");
                        ps.setString (1, txtID.getText());
                        ps.setString (2, txtUnit.getText());
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Record Saved Successfully");
                        table2.FillTable(jTable3, unit, txtSearch2);
                        
                        clrField(Units);
                        
                          } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }//GEN-LAST:event_btnSave2MouseClicked

    private void txtUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUnitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUnitActionPerformed

    private void txtIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDActionPerformed

    private void txtSearch3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch3CaretUpdate
        // TODO add your handling code here:
        table2.FillTable(jTable1, unit, txtSearch3);
    }//GEN-LAST:event_txtSearch3CaretUpdate

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel5MouseClicked

    private void txtSearch4CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch4CaretUpdate
        // TODO add your handling code here:
         table2.FillTable(jTable4, rooms, txtSearch4);
    }//GEN-LAST:event_txtSearch4CaretUpdate

    public void FillTable(JTable table, String Query)
{
    try
    {
        con1.init();
       con1.pst = con1.cn.prepareStatement(Query);
        con1.rs = con1.pst.executeQuery();
        
        //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = con1.rs.getMetaData().getColumnCount();
        while(con1.rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = con1.rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(con1.rs.getRow()-1,row);
        }

        con1.cn.close();
    }
    catch(SQLException e)
    {
        JOptionPane.showMessageDialog(this,e);
    }
}   
    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jTable5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable5MouseClicked
        // TODO add your handling code here:
        jDialog1.setLocationRelativeTo(null); 
        FillTable(jTable4, "SELECT * FROM `room`");
        FillTable(jTable1, "SELECT * FROM `units`");
        jDialog1.setVisible(true);
       txtRoom1.setText("");
        txtRoom2.setText("");
        
    }//GEN-LAST:event_jTable5MouseClicked

    void Start()
    {
        FillARooms(jTable5);
         getTmtb(cboGroup.getSelectedItem().toString());

    }
    
    private void txtRoom1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoom1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRoom1ActionPerformed

    private void txtRoom2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoom2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRoom2ActionPerformed

    private void btnSave1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave1MouseClicked
        // TODO add your handling code here:
        

        Connection con = dbconnect.connect();
        int rw = jTable5.getSelectedRow();
                        int cl = jTable5.getSelectedColumn();
                        String theID = jTable5.getValueAt(rw, cl).toString();
                        String result = "null";
if (theID.contains(":")) {
    int firstColon = theID.indexOf(':');
int secondColon = theID.indexOf('.', firstColon+1);
if (secondColon != -1) {
    result = theID.substring(firstColon+1, secondColon).trim();
} 
} else {
    result = "ERROR"; // or handle this however you want
}
                        if("".equals(jTable5.getValueAt(rw, cl).toString()) || jTable5.getValueAt(rw, cl).toString() == null)
                        {
            try {
                ps = con.prepareStatement("INSERT INTO `class`(`Course`, `Unit`, `Room`, `Day`, `Time`) VALUES (?, ?, ?, ?, ?)");
               ps.setString (1, cboGroup.getSelectedItem().toString());
                        ps.setString (2, txtRoom1.getText());
                        ps.setString (3, txtRoom2.getText());
                        
                        String Dd = "null";
                        String tm = "null";
                switch (rw) {
                    case 0:
                        Dd = "Monday";
                        break;
                    case 1:
                        Dd = "Tuesday";
                        break;
                    case 2:
                        Dd = "Wednesday";
                        break;
                    case 3:
                        Dd = "Thursday";
                        break;
                    case 4:
                        Dd = "Friday";
                        break;
                    default:
                        break;
                }
                
                 switch (cl) {
                    case 0:
                        tm = Times[0];
                        break;
                    case 1:
                        tm = Times[1];
                        break;
                    case 2:
                        tm = Times[2];
                        break;
                    case 3:
                        tm = Times[3];
                        break;
                    case 4:
                        tm = Times[4];
                        break;
                        case 5:
                        tm = Times[5];
                        break;
                        case 6:
                        tm = Times[6];
                        break;
                        case 7:
                        tm = Times[7];
                        break;
                        case 8:
                        tm = Times[8];
                        break;
                    default:
                        break;
                }
                
                        ps.setString (4, Dd);
                        ps.setString (5, tm);
                        ps.executeUpdate();
            } catch (SQLException e) {
                
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                        }
                        else
                        {
                           
                            try {
            ps = con.prepareStatement("UPDATE `class` SET `Course`= ?, " +
                   "`Unit`= ?,`Room`= ?,`Day`= ?,`Time`= ? WHERE CID = ?");
               ps.setString (1, cboGroup.getSelectedItem().toString());
                        ps.setString (2, txtRoom1.getText());
                        ps.setString (3, txtRoom2.getText());
                        
                        String Dd = "null";
                        String tm = "null";
                switch (rw) {
                    case 0:
                        Dd = "Monday";
                        break;
                    case 1:
                        Dd = "Tuesday";
                        break;
                    case 2:
                        Dd = "Wednesday";
                        break;
                    case 3:
                        Dd = "Thursday";
                        break;
                    case 4:
                        Dd = "Friday";
                        break;
                    default:
                        break;
                }
                
                 switch (cl) {
                    case 0:
                        tm = Times[0];
                        break;
                    case 1:
                        tm = Times[1];
                        break;
                    case 2:
                        tm = Times[2];
                        break;
                    case 3:
                        tm = Times[3];
                        break;
                    case 4:
                        tm = Times[4];
                        break;
                        case 5:
                        tm = Times[5];
                        break;
                        case 6:
                        tm = Times[6];
                        break;
                        case 7:
                        tm = Times[7];
                        break;
                        case 8:
                        tm = Times[8];
                        break;
                    default:
                        break;
                }
                
                        ps.setString (4, Dd);
                        ps.setString (5, tm);
               ps.setString (6, result);
            ps.executeUpdate();
            con.close();
        }
        catch (SQLException ex ) {
            JOptionPane.showMessageDialog(null, ex);

        }

                        }
            FillARooms(jTable5);
        getTmtb(cboGroup.getSelectedItem().toString());
        jDialog1.dispose();
    }//GEN-LAST:event_btnSave1MouseClicked

    private void btnDel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel1MouseClicked
        // TODO add your handling code here:
        txtRoom1.setText(null);
        txtRoom2.setText(null);
        jDialog1.dispose();
    }//GEN-LAST:event_btnDel1MouseClicked

    private void jLabel42MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel42MouseClicked
        // TODO add your handling code here:
        Room.setLocationRelativeTo(null);
        Room.setVisible(true);
    }//GEN-LAST:event_jLabel42MouseClicked

    private void jLabel12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseClicked
        // TODO add your handling code here:
        Unit.setLocationRelativeTo(null);
        Unit.setVisible(true);
        
        
    }//GEN-LAST:event_jLabel12MouseClicked

    private void txtSearch5CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch5CaretUpdate
        // TODO add your handling code here:
        table2.FillTable(jTable6, booking, txtSearch5);
    }//GEN-LAST:event_txtSearch5CaretUpdate

    private void jTable6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable6MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable6MouseClicked

    private void jLabel45MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel45MouseClicked
        // TODO add your handling code here:
        
        if (jDateChooser6.getDate() == null || jDateChooser6.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Please set the required dates");
        } else {
            
            FilterTable(jTable6, filter, jDateChooser6.getDate(), jDateChooser7.getDate());
        }
    }//GEN-LAST:event_jLabel45MouseClicked

    private void jLabel47MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel47MouseClicked
        // TODO add your handling code here:
        
        BPanel(Manager);
        BPanel(History);
        BPanel(Units);
        BPanel(Rooms);
        TPanel(Pass);
        
        jPanel4.setVisible(false);
    }//GEN-LAST:event_jLabel47MouseClicked

    private void jLabel48MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel48MouseClicked
        // TODO add your handling code here:
         Login login = new Login();
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLabel48MouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        if(jPanel4.isVisible())
        jPanel4.setVisible(false);
        else
         jPanel4.setVisible(true);
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel47MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel47MouseDragged
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jLabel47MouseDragged

    private void jLabel47MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel47MouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel47MouseMoved

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int selectedRowIndex = jTable1.getSelectedRow();
        txtRoom1.setText(model.getValueAt(selectedRowIndex, 2).toString());
        Unit.dispose();
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable4MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel) jTable4.getModel();
        int selectedRowIndex = jTable4.getSelectedRow();
        if(txtRoom2.getText().equals("") || txtRoom2.getText() == null)
        {
            txtRoom2.setText(model.getValueAt(selectedRowIndex, 1).toString());
        }
        else
            txtRoom2.setText(txtRoom2.getText()+","+model.getValueAt(selectedRowIndex, 1).toString());
        
        Room.dispose();
    }//GEN-LAST:event_jTable4MouseClicked

    private void btnSave3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave3MouseClicked
        // TODO add your handling code here:
        int rw = jTable5.getSelectedRow();
                        int cl = jTable5.getSelectedColumn();
                        String theID = jTable5.getValueAt(rw, cl).toString();
                        String result = "null";
if (theID.contains(":")) {
    int firstColon = theID.indexOf(':');
int secondColon = theID.indexOf('.', firstColon+1);
if (secondColon != -1) {
    result = theID.substring(firstColon+1, secondColon).trim();
} 
} else {
    result = "ERROR"; // or handle this however you want
}
        Connection con = dbconnect.connect();
        try {
            ps = con.prepareStatement("DELETE FROM `class` WHERE `CID` = ?");
            ps.setString(1, result);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Lesson Deleted Successfully");
         
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
                try {
                    con.close();
                            } catch (SQLException ex) {
                    Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
          FillARooms(jTable5);
        getTmtb(cboGroup.getSelectedItem().toString());
        jDialog1.dispose();
        
    }//GEN-LAST:event_btnSave3MouseClicked

    private void jLabel5KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel5KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel5KeyReleased

    private void jTable2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable2MouseEntered

    private void jLabel38MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel38MouseClicked
        // TODO add your handling code here:
        int input = JOptionPane.showConfirmDialog(null, "This action will delete all the saved TimeTables. Do oyu wish to continue?", "Confirm",JOptionPane.OK_CANCEL_OPTION);
        if(input == JOptionPane.OK_OPTION){
        Connection con = dbconnect.connect();
        try {
            ps = con.prepareStatement("TRUNCATE class");
            ps.executeUpdate();
       JOptionPane.showMessageDialog(null, "All Timetables Deleted Successfully");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
    }//GEN-LAST:event_jLabel38MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
         try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Admin_Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Footer;
    private javax.swing.JPanel Header;
    private javax.swing.JPanel History;
    private javax.swing.JPanel Home;
    private javax.swing.JPanel Manager;
    private javax.swing.JPanel NavBar;
    private javax.swing.JPanel Pass;
    private javax.swing.JDialog Room;
    private javax.swing.JPanel Rooms;
    private javax.swing.JDialog Unit;
    private javax.swing.JPanel Units;
    private javax.swing.JLabel btnDel;
    private javax.swing.JLabel btnDel1;
    private javax.swing.JLabel btnDel2;
    private javax.swing.JLabel btnHistory;
    private javax.swing.JLabel btnManager;
    private javax.swing.JLabel btnReset;
    private javax.swing.JLabel btnReset2;
    private javax.swing.JLabel btnRooms;
    private javax.swing.JLabel btnSave;
    private javax.swing.JLabel btnSave1;
    private javax.swing.JLabel btnSave2;
    private javax.swing.JLabel btnSave3;
    private javax.swing.JLabel btnUnits;
    private javax.swing.JLabel btnUpdate;
    private javax.swing.JLabel btnUpdate2;
    private javax.swing.JComboBox<String> cboBuilding;
    private javax.swing.JComboBox<String> cboGroup;
    private javax.swing.JCheckBox jCheckBox1;
    private com.toedter.calendar.JDateChooser jDateChooser6;
    private com.toedter.calendar.JDateChooser jDateChooser7;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTextField txtCapacity;
    private javax.swing.JPasswordField txtConfirm;
    private javax.swing.JPasswordField txtCurr;
    private javax.swing.JTextField txtID;
    private javax.swing.JPasswordField txtNewPass;
    private javax.swing.JTextField txtRoom;
    private javax.swing.JTextField txtRoom1;
    private javax.swing.JTextField txtRoom2;
    private javax.swing.JTextField txtSearch1;
    private javax.swing.JTextField txtSearch2;
    private javax.swing.JTextField txtSearch3;
    private javax.swing.JTextField txtSearch4;
    private javax.swing.JTextField txtSearch5;
    private javax.swing.JTextField txtUnit;
    // End of variables declaration//GEN-END:variables
public void fillCombo (JComboBox combo, String sql, String column) {
        Connection con = dbconnect.connect();
        combo.removeAllItems();
    try {
    ps = con.prepareStatement(sql);
    rs = ps.executeQuery();
    while (rs.next ()) {
    String name = rs.getString(column);
    combo.addItem (name);
    }
    } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        } finally {
            try {
                con.close ();
            } catch (SQLException ex) {
                Logger.getLogger(Admin_Menu.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}
}

