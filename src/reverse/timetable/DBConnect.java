package reverse.timetable;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ruth
 */
public class DBConnect {
    private Connection con = null;
    
        public Connection connect(){
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
              con = DriverManager.getConnection("jdbc:mysql://localhost:3306/rtime", "root", "");
              return con;
            }catch(ClassNotFoundException classNotFoundException){
                return null;
            } catch(SQLException ex){
                return null;
            }catch (Exception ex){
                return null;
            }
        }
       public void closeDatabase(){
           if (con != null){
               try{
                    con.close();
               }catch(SQLException sqleex){
               
               }catch (Exception ex){
                   //you can log some custome messages using the getMessage() method.
               }
           }
       }
}
